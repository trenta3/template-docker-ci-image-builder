# Example Template for Repositories building docker images
This is a repository that shows how to build a docker image using GitLab CI and then upload it to the GitLab Container Registry of the same repository.

## QuickStart
1. Add the variable `GITLAB_REGISTRY_TOKEN` to the CI/CD settings in GitLab, after obtaining a personal token with write access to the container registry from the GitLab personal settings.
2. Modify the `Dockerfile` to build the required image and `test/test.sh` to test it (or just make it do nothing if you don't want to test your image).
3. Push to GitLab and enjoy the automatic building based on tags and branch names.

Don't forget to edit this README file, and further adjust the settings according to your own needs.
